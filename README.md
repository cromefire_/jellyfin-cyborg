# Jellyfin Project Cyborg

[![pipeline status](https://gitlab.com/cromefire_/jellyfin-cyborg/badges/master/pipeline.svg)](https://gitlab.com/cromefire_/jellyfin-cyborg/-/commits/master)
[![license](https://img.shields.io/badge/license-MPL--2.0-important)](https://gitlab.com/cromefire_/jellyfin-cyborg/-/blob/master/LICENSE.md)
