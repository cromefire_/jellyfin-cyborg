class NativeError extends Error {
    /**
     * @param name {String?}
     * @param message {String?}
     */
    constructor(name, message) {
        super(message);

        this.name = name
    }
}

window.connector = {
    /**
     * @public
     *
     * @param name {String}
     * @param args {*}
     */
    callAsync(name, args) {
        const id = this.idCounter++;
        const prom = new Promise((resolve, reject) => {
            /**
             * @param isError {Boolean}
             * @param ret {*}
             */
            this.callbacks[0] = (isError, ret) => {
                if (isError) {
                    reject(ret)
                } else {
                    resolve(ret)
                }
            }
        })
        args = args ? args : null
        nativeConnector.callAsync(name, JSON.stringify(args), id)
        return prom
    },
    /**
     * @public
     *
     * @param name {String}
     * @param args {*?}
     * @return {*}
     */
    call(name, args) {
        args = args ? args : null
        const ret = nativeConnector.call(name, JSON.stringify(args))
        const obj = JSON.parse(ret)
        if (obj.isError) {
            throw NativeError(obj.value.name, obj.value.message)
        }
        return obj.value
    },
    /**
     * @internal
     */
    callback(id, isError, ret) {
        this.callbacks[id](isError, JSON.parse(ret))
        delete this.callbacks[id]
    },
    /**
     * @private
     */
    idCounter: 0,
    /**
     * @private
     */
    callbacks: {}
}

/*window.NativeShell = {
    AppHost: {
        init() {
            connector.call("AppHost.init")
        }
    }
}*/

console.info("Injected glue code");
