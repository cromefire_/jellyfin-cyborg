/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package org.jellyfin.android.cyborg.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewStub
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import org.jellyfin.android.cyborg.R
import org.jellyfin.android.cyborg.databinding.FragmentHomeBinding
import org.jellyfin.android.cyborg.databinding.HomeRowBinding

class HomeFragment : Fragment() {
    private val homeViewModel by viewModels<HomeViewModel>()

    private lateinit var binding: FragmentHomeBinding

    private lateinit var row: HomeRowBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentHomeBinding.inflate(inflater, container, false)

        val stub = ViewStub(requireContext())
        stub.layoutResource = R.layout.home_row
        binding.rowList.addView(stub)
        val rowView = stub.inflate()

        row = HomeRowBinding.bind(rowView)
        row.title.text = "This is the first row"

        return binding.root
    }
}
