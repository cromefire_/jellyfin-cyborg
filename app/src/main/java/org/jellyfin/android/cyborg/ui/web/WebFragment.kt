/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package org.jellyfin.android.cyborg.ui.web

import android.annotation.SuppressLint
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.launch
import org.intellij.lang.annotations.Language
import org.jellyfin.android.cyborg.BuildConfig
import org.jellyfin.android.cyborg.R
import org.jellyfin.android.cyborg.databinding.WebFragmentBinding
import org.jellyfin.android.cyborg.di.services.AuthService
import org.koin.android.ext.android.inject
import org.mozilla.geckoview.GeckoRuntime
import org.mozilla.geckoview.GeckoRuntimeSettings
import org.mozilla.geckoview.GeckoSession
import org.mozilla.geckoview.GeckoSession.PermissionDelegate.*
import org.mozilla.geckoview.GeckoSessionSettings

class CyborgWebPermissions : GeckoSession.PermissionDelegate {
    override fun onContentPermissionRequest(
        session: GeckoSession, url: String?, permission: Int, callback: Callback
    ) {
        when (permission) {
            PERMISSION_GEOLOCATION -> callback.reject()
            PERMISSION_DESKTOP_NOTIFICATION -> callback.grant()
            PERMISSION_PERSISTENT_STORAGE -> callback.grant()
            PERMISSION_XR -> callback.reject()
            PERMISSION_AUTOPLAY_INAUDIBLE -> callback.grant()
            PERMISSION_AUTOPLAY_AUDIBLE -> callback.grant()
            PERMISSION_MEDIA_KEY_SYSTEM_ACCESS -> callback.grant()
        }
    }
}

class WebFragment : Fragment() {
    private val authService by inject<AuthService>()

    private val viewModel by viewModels<WebViewModel>()
    private lateinit var binding: WebFragmentBinding
    private val geckoview by lazy { binding.geckoview }
    private lateinit var session: GeckoSession
    private lateinit var runtime: GeckoRuntime

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        binding = WebFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    @SuppressLint("SetJavaScriptEnabled")
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val sessionSettings =
            GeckoSessionSettings.Builder()
                .allowJavascript(true)
                .userAgentOverride("Mozilla/5.0 (Android ${Build.VERSION.RELEASE}) JellyfinCyborg/${BuildConfig.VERSION_NAME}")
                .build()

        val runtimeSettings =
            GeckoRuntimeSettings.Builder()
                .aboutConfigEnabled(BuildConfig.DEBUG)
                .remoteDebuggingEnabled(BuildConfig.DEBUG)
                .javaScriptEnabled(true)
                .build()

        session = GeckoSession(sessionSettings)
        runtime = GeckoRuntime.create(requireContext(), runtimeSettings)

        session.open(runtime)
        geckoview.autofillEnabled = false
        geckoview.setSession(session)

        session.permissionDelegate = CyborgWebPermissions()
        session.loadUri("${authService.requireCurrentApiCredentials().address}/web/index.html")

        // Fixme: Communication and injection
        val script = resources.openRawResource(R.raw.web_connector).bufferedReader().lineSequence()
                .joinToString("\n")

        lifecycleScope.launch {
            val cred = authService.requireCurrentApiCredentials()
            // TODO: Correct server name
            @Language("JSON") val json = """
            {
              "Servers": [
                {
                  "ManualAddress": "${cred.address}",
                  "ManualAddressOnly": true,
                  "Name": "App Server",
                  "Id": "${cred.serverId}",
                  "UserId": "${cred.userId}",
                  "AccessToken": "${cred.token}"
                }
              ]
            }
            """.trimIndent()
        }
    }

    companion object {
        fun newInstance() = WebFragment()
    }
}
