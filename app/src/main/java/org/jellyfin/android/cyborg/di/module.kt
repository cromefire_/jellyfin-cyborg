/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package org.jellyfin.android.cyborg.di

import org.jellyfin.android.cyborg.di.services.ApiService
import org.jellyfin.android.cyborg.di.services.AuthService
import org.koin.dsl.module

val appModule = module {
    single {
        AuthService()
    }
    single {
        ApiService()
    }
}
