/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package org.jellyfin.android.cyborg.glue

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonConfiguration

class NativeConnector(
    private val scope: CoroutineScope,
    private val nativeShell: NativeShell,
    private val asyncReturn: suspend (id: Int, isError: Boolean, value: String?) -> Unit
) {
    private val json = Json(
        JsonConfiguration.Stable.copy(
            ignoreUnknownKeys = true
        )
    )

    fun callAsync(method: String, args: String, returnId: Int) {
        scope.launch {
            val ret = try {
                routeAsync(method, args)
            } catch (e: Throwable) {
                val err = json.stringify(
                    NativeError.serializer(), NativeError(
                        e::class.simpleName, e.message
                    )
                )
                asyncReturn(returnId, true, err)
                return@launch
            }
            asyncReturn(returnId, false, ret)
        }
    }

    fun call(method: String, args: String): String {
        val ret = try {
            route(method, args)
        } catch (e: Throwable) {
            val err = json.stringify(
                NativeError.serializer(), NativeError(
                    e::class.simpleName, e.message
                )
            )
            return "{\"isError\":true,\"value\":$err}"
        }
        return "{\"isError\":false,\"value\":$ret}"
    }

    private suspend fun routeAsync(method: String, args: String): String? {
        when (method) {
            else -> throw IllegalStateException("Method \"${method}\" not known")
        }
        return null
    }

    private fun route(method: String, args: String): String? {
        when (method) {
            "AppHost.init" -> nativeShell.appHost.init()
            else -> throw IllegalStateException("Method \"${method}\" not known")
        }
        return null
    }
}
